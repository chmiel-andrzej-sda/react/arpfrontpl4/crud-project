import { mockNavigate } from '../__mocks__/MockReactRouterDom';
import { shallow, ShallowWrapper } from 'enzyme';
import React from 'react';
import { MainPage } from '../../src/pages/MainPage';

describe('MainPage', (): void => {
	it('renders with token', (): void => {
		// when
		const wrapper: ShallowWrapper = shallow(
			<MainPage token='test-token' />
		);

		// then
		expect(wrapper).toMatchSnapshot();
	});

	it('renders without token', (): void => {
		// when
		const wrapper: ShallowWrapper = shallow(<MainPage />);

		// then
		expect(wrapper).toMatchSnapshot();
	});

	it('opens dialog', (): void => {
		// given
		const wrapper: ShallowWrapper = shallow(
			<MainPage token='test-token' />
		);

		// when
		wrapper.find('.delete-users').simulate('click');

		// then
		expect(wrapper).toMatchSnapshot();
	});

	it('closes dialog', (): void => {
		// given
		const wrapper: ShallowWrapper = shallow(
			<MainPage token='test-token' />
		);

		wrapper.find('.delete-users').simulate('click');

		// when
		wrapper.find('.dialog').simulate('close');

		// then
		expect(wrapper).toMatchSnapshot();
	});

	it('accepts dialog', (): void => {
		// given
		const wrapper: ShallowWrapper = shallow(
			<MainPage token='test-token' />
		);

		wrapper.find('.delete-users').simulate('click');

		// when
		wrapper.find('.accept').simulate('click');

		// then
		expect(wrapper).toMatchSnapshot();
	});

	it('handles login click', (): void => {
		// given
		const wrapper: ShallowWrapper = shallow(
			<MainPage token='test-token' />
		);

		// when
		wrapper.find('.navigate').simulate('click');

		// then
		expect(mockNavigate).toHaveBeenCalledTimes(1);
	});

	it('handles change', (): void => {
		// given
		const wrapper: ShallowWrapper = shallow(
			<MainPage token='test-token' />
		);

		// when
		wrapper.find('.text-add').simulate('change', {
			target: {
				value: 'abc'
			}
		});

		// then
		expect(mockNavigate).toHaveBeenCalledTimes(1);
	});

	it('handles correct add', (): void => {
		// given
		const wrapper: ShallowWrapper = shallow(
			<MainPage token='test-token' />
		);

		wrapper.find('.text-add').simulate('change', {
			target: {
				value: 'abc'
			}
		});

		// when
		wrapper.find('.button-add').simulate('click');

		// then
		expect(mockNavigate).toHaveBeenCalledTimes(1);
	});

	it('handles incorrect add', (): void => {
		// given
		const wrapper: ShallowWrapper = shallow(
			<MainPage token='test-token' />
		);

		wrapper.find('.text-add').simulate('change', {
			target: {
				value: 'users'
			}
		});

		// when
		wrapper.find('.button-add').simulate('click');

		// then
		expect(mockNavigate).toHaveBeenCalledTimes(1);
	});
});
