import { shallow, ShallowWrapper } from 'enzyme';
import React from 'react';
import { AddPersonFormPage } from '../../src/pages/AddPersonFormPage';

describe('AddPersonFormPage', (): void => {
	it('renders without token', (): void => {
		// when
		const wrapper: ShallowWrapper = shallow(
			<AddPersonFormPage resource='users' />
		);

		// then
		expect(wrapper).toMatchSnapshot();
	});

	it('renders with token', (): void => {
		// when
		const wrapper: ShallowWrapper = shallow(
			<AddPersonFormPage
				resource='users'
				token='test-token'
			/>
		);

		// then
		expect(wrapper).toMatchSnapshot();
	});

	it('handles change', (): void => {
		// given
		const wrapper: ShallowWrapper = shallow(
			<AddPersonFormPage
				resource='users'
				token='test-token'
			/>
		);

		// when
		wrapper.find('.text-first-name').simulate('change', {
			target: {
				value: 'first-name'
			}
		});

		wrapper.find('.text-last-name').simulate('change', {
			target: {
				value: 'last-name'
			}
		});

		wrapper.find('.text-email').simulate('change', {
			target: {
				value: 'test@test.pl'
			}
		});

		wrapper.find('.text-url').simulate('change', {
			target: {
				value: 'url'
			}
		});

		// then
		expect(wrapper).toMatchSnapshot();
	});

	it('handles submit', (): void => {
		// given
		const wrapper: ShallowWrapper = shallow(
			<AddPersonFormPage
				resource='users'
				token='test-token'
			/>
		);

		wrapper.find('.text-first-name').simulate('change', {
			target: {
				value: 'first-name'
			}
		});

		wrapper.find('.text-last-name').simulate('change', {
			target: {
				value: 'last-name'
			}
		});

		wrapper.find('.text-email').simulate('change', {
			target: {
				value: 'test@test.pl'
			}
		});

		wrapper.find('.text-url').simulate('change', {
			target: {
				value: 'url'
			}
		});

		// when
		wrapper.find('.button-send').simulate('click');

		// then
		expect(wrapper).toMatchSnapshot();
	});

	it('handles incorrect data', (): void => {
		// given
		const wrapper: ShallowWrapper = shallow(
			<AddPersonFormPage
				resource='users'
				token='test-token'
			/>
		);

		wrapper.find('.text-email').simulate('change', {
			target: {
				value: 'test'
			}
		});

		// when
		wrapper.find('.button-send').simulate('click');

		// then
		expect(wrapper).toMatchSnapshot();
	});

	it('handles incorrect and then correct data', (): void => {
		// given
		const wrapper: ShallowWrapper = shallow(
			<AddPersonFormPage
				resource='users'
				token='test-token'
			/>
		);

		wrapper.find('.text-email').simulate('change', {
			target: {
				value: 'test'
			}
		});

		wrapper.find('.button-send').simulate('click');

		wrapper.find('.text-first-name').simulate('change', {
			target: {
				value: 'first-name'
			}
		});

		wrapper.find('.text-last-name').simulate('change', {
			target: {
				value: 'last-name'
			}
		});

		wrapper.find('.text-email').simulate('change', {
			target: {
				value: 'test@test.pl'
			}
		});

		wrapper.find('.text-url').simulate('change', {
			target: {
				value: 'url'
			}
		});

		// when
		wrapper.find('.button-send').simulate('click');

		// then
		expect(wrapper).toMatchSnapshot();
	});
});
