import { mockUseParams } from '../__mocks__/MockReactRouterDom';
import { shallow, ShallowWrapper } from 'enzyme';
import React from 'react';
import { SingleResourcePage } from '../../src/pages/SingleResourcePage';

describe('SingleResourcePage', (): void => {
	it('renders without resource', (): void => {
		// given
		mockUseParams.mockReturnValue({ id: '1' });
		jest.spyOn(React, 'useState').mockReturnValue([
			{ id: 'test' },
			(): void => undefined
		]);

		// when
		const wrapper: ShallowWrapper = shallow(
			<SingleResourcePage
				resource='users'
				token='test-token'
			/>
		);

		// then
		expect(wrapper).toMatchSnapshot();
	});
});
