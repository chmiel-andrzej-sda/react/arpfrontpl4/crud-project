export const mockUseParams: jest.Mock = jest.fn();
export const mockNavigate: jest.Mock = jest.fn();

jest.mock('react-router-dom', () => ({
	useParams: mockUseParams,
	useNavigate: (): (() => void) => mockNavigate,
	Route: (): void => undefined,
	Routes: (): void => undefined
}));
