import React from 'react';
import axios, { AxiosResponse, AxiosError } from 'axios';
import { useParams } from 'react-router-dom';
import { APISingleResponse } from '../interfaces/APISIngleResponse';
import Skeleton from '@mui/material/Skeleton';
import Button from '@mui/material/Button';
import { useNavigate, NavigateFunction } from 'react-router-dom';
import TextField from '@mui/material/TextField';
import { BackButton } from '../components/BackButton';

export interface SingleResourcePageProps {
	resource: string;
	token?: string;
}

export function SingleResourcePage<R extends { id: string | number }>(
	props: SingleResourcePageProps
): JSX.Element {
	const [resource, setResource] = React.useState<R | undefined>(undefined);
	const [newResource, setNewResource] = React.useState<R | undefined>(
		undefined
	);

	const params = useParams();
	const navigate: NavigateFunction = useNavigate();

	React.useEffect((): void => {
		axios
			.get(`https://reqres.in/api/${props.resource}/${params.id}`)
			.then((response: AxiosResponse<APISingleResponse<R>>): void => {
				setResource(response.data.data);
			})
			.catch((error: AxiosError): void => {
				if (error.response?.status === 404) {
					navigate('/404');
				}
			});
	}, []);

	function handleDeleteClick(): void {
		axios
			.delete(`https://reqres.in/api/${props.resource}/${params.id}`)
			.then((response: AxiosResponse<{}>): void => {
				if (response.status === 204) {
					navigate(`/${props.resource}`);
				}
			});
	}

	function handleEditClick(): void {
		if (!resource) {
			return;
		}
		setNewResource(resource);
	}

	function handlePropertyChange(text: string, key: string): void {
		if (!newResource) {
			return;
		}
		// @ts-ignore
		newResource[key] = text;
		setNewResource({ ...newResource });
	}

	function renderPropery(key: string): JSX.Element | string {
		if (newResource) {
			return (
				<div key={key}>
					<TextField
						label={key}
						variant='outlined'
						// @ts-ignore
						value={newResource[key]}
						onChange={(
							event: React.ChangeEvent<HTMLInputElement>
						): void =>
							handlePropertyChange(event.target.value, key)
						}
					/>
				</div>
			);
		}
		// @ts-ignore
		return <div key={key}>{resource?.[key]}</div>;
	}

	function handleCancelClick(): void {
		setNewResource(undefined);
	}

	function handleOkClick(): void {
		axios
			.put(
				`https://reqres.in/api/${props.resource}/${params.id}`,
				newResource
			)
			.finally(() => {
				if (!resource) {
					return;
				}
				setResource({
					...resource,
					...newResource
				});
				setNewResource(undefined);
			});
	}

	function renderButtons(): JSX.Element {
		if (newResource) {
			return (
				<>
					<Button
						variant='contained'
						onClick={handleOkClick}
						// disabled={firstName === "" || lastName === "" || email === ""}
					>
						OK
					</Button>
					<Button
						variant='outlined'
						onClick={handleCancelClick}
						color='error'
					>
						Cancel
					</Button>
				</>
			);
		}
		if (!props.token) {
			return <></>;
		}
		return (
			<>
				<Button
					variant='contained'
					onClick={handleEditClick}
				>
					Edit
				</Button>
				<Button
					variant='contained'
					onClick={handleDeleteClick}
					color='error'
				>
					Delete
				</Button>
			</>
		);
	}

	if (!resource) {
		return (
			<div>
				<BackButton to={`/${props.resource}`} />
				<Skeleton
					variant='text'
					width={115}
					height={20}
				/>
				<Skeleton
					variant='text'
					width={65}
					height={20}
				/>
				<Skeleton
					variant='rectangular'
					width={100}
					height={100}
				/>
			</div>
		);
	}

	return (
		<div>
			<BackButton to={`/${props.resource}`} />
			Resource #{resource?.id}
			{Object.keys(resource)
				.filter((key: string): boolean => key !== 'id')
				.map(renderPropery)}
			{renderButtons()}
		</div>
	);
}
