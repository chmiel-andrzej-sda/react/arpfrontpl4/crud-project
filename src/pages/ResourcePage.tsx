import { useParams, Routes, Route } from 'react-router-dom';
import { AddPersonFormPage } from './AddPersonFormPage';
import { ResourceListPage } from './PeopleListPage';
import { SingleResourcePage } from './SingleResourcePage';

type ResourceParams = {
	readonly resource: string;
};

export interface ResourcePageProps {
	readonly token?: string;
}

export function ResourcePage(props: ResourcePageProps): JSX.Element {
	const params: Readonly<Partial<ResourceParams>> =
		useParams<ResourceParams>();

	const target: string = params.resource || 'users';

	return (
		<Routes>
			<Route
				path='/'
				element={
					<ResourceListPage
						resource={target}
						token={props.token}
					/>
				}
			/>
			<Route
				path='/add'
				element={
					<AddPersonFormPage
						resource={target}
						token={props.token}
					/>
				}
			/>
			<Route
				path='/:id'
				element={
					<SingleResourcePage
						resource={target}
						token={props.token}
					/>
				}
			/>
		</Routes>
	);
}
