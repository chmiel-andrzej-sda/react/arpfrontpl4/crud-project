import { useNavigate, NavigateFunction } from 'react-router-dom';
import Button from '@mui/material/Button';

export function Error404Page(): JSX.Element {
	const navigate: NavigateFunction = useNavigate();

	return (
		<div>
			Erorr 404: Not found!
			<br />
			<Button onClick={() => navigate('/')}>&lt; Main Page</Button>
		</div>
	);
}
